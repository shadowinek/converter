<?php

    require_once('vendor/autoload.php');
    
    $finder = new Symfony\Component\Finder\Finder();
    $files = $finder->files()
                    ->name('*.doc')
                    ->in(__DIR__ . '/input/');
                    
    $python = realpath('vendor/dagwieers/unoconv/unoconv');
    
    $output = realpath(__DIR__ . '/output/');                              
                    
    foreach ($files as $file) {            
        $out = exec("python \"{$python}\" -f odt -o \"{$output}\" \"{$file->getRealPath()}\"");
        echo $file->getFileName() . "\n";
    }               